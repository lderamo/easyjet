include: base-config.yaml

# Toggles for object types
do_muons: true
do_electrons: true
do_small_R_jets: true
do_large_R_UFO_jets: true
# No b-tagging recommendation available for VR jets in p5855 samples
do_VR_jets: false

# Decay mode considered for truth decoration
truthDecayModes: ["bbbb"]

# small R config
small_R:
  # nominal btagging working point, used for b-jet pT calibration in particular
  btag_wp: "DL1dv01_FixedCutBEff_77"
  # additional btagging working points, available for selections and per-jet SFs
  btag_extra_wps:
    - DL1dv01_FixedCutBEff_70
    - DL1dv01_FixedCutBEff_85
    - GN120220509_FixedCutBEff_70
    - GN120220509_FixedCutBEff_77
    - GN120220509_FixedCutBEff_85

# large R config
large_R:
  # btagging working points for Variable Radius Track jets
  vr_btag_wps:
    - DL1r_FixedCutBEff_77
    - DL1r_FixedCutBEff_85

# list of triggers per year to consider
trigger_chains:
  include: trigger.yaml
# apply trigger lists to filter events
do_trigger_filtering: true
# apply loose jet cleaning only
loose_jet_cleaning: true
# write objects with overlap removal applied
# turning this on you have to decide on one large R jet collection
do_overlap_removal: true
# enable all systematics
systematics_regex: [".*"]

# Include the TTree configuration and update details
ttree_output:
  - 
    include: AnalysisMiniTree-config.yaml
    # Our overrides. All defaults are off
    reco_outputs:
      muons: 'container_names.output.muons'
      electrons: 'container_names.output.electrons'
      small_R_jets: 'container_names.output.reco4PFlowJet'
      large_R_UFO_jets: 'container_names.output.reco10UFOJet'
      #VR_jets: 'container_names.output.vrJets'
    collection_options:
      small_R_jets:
        higgs_parent_info: true
        btag_info: true
        JVT_details: true
        no_bjet_calib_p4: true
        gn2_branches: true
      large_R_jets:
        substructure_info: true
        truth_labels: true
    truth_outputs:
      small_R_jets: 'container_names.input.truth4Jet'
      large_R_jets: 'container_names.input.truth10SoftDropJet'
      higgs_particle: 'container_names.output.truthHHParticles'

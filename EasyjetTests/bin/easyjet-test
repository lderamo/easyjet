#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

#tag from easyjet/hh4b-test-files
TAG=p5855HHbbyy

# standard samples
PHYS_DATA_2018=data18_13TeV.00362204.physics.DAOD_PHYS_10evts.r13286_p5855.pool.root
PHYSLITE_DATA_2018=data18_13TeV.00362204.physics.DAOD_PHYSLITE_10evts.r13286_p5855.pool.root
PHYS_DATA_2022=data22_13p6TeV.00436656.physics_Main.DAOD_PHYS_10evts.r14190_p5858.pool.root
PHYSLITE_DATA_2022=data22_13p6TeV.00436656.physics_Main.DAOD_PHYSLITE_10evts.r14190_p5858.pool.root
PHYS_JETS_MC20=mc20_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.DAOD_PHYS_10evts.e7142_s3681_r13145_p5855.pool.root
PHYS_TTBAR_MC20=mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.DAOD_PHYS_10evts.e6337_s3681_r13145_p5855.pool.root
PHYS_SH4B_BOOSTED_MC20=mc20_13TeV.801636.Py8EG_A14NNPDF23LO_XHS_X3000_S70_4b.DAOD_PHYS_10evts.e8448_a899_r13145_p5631.pool.root
PHYS_SH4B_BOOSTED_MC20_p5658=mc20_13TeV.801637.Py8EG_A14NNPDF23LO_XHS_X3000_S100_4b.DAOD_PHYS_10evts.e8448_a899_r13145_p5658.pool.root
PHYSLITE_SH4B_BOOSTED_MC20=mc20_13TeV.801636.Py8EG_A14NNPDF23LO_XHS_X3000_S70_4b.DAOD_PHYSLITE_10evts.e8448_a899_r13145_p5631.pool.root
PHYS_HH4B_MC21=mc21_13p6TeV.601479.PhPy8EG_HH4b_cHHH01d0.DAOD_PHYS_10evts.e8472_s3873_r13829_p5631.pool.root
PHYSLITE_HH4B_MC21=mc21_13p6TeV.601479.PhPy8EG_HH4b_cHHH01d0.DAOD_PHYSLITE_10evts.e8472_s3873_r13829_p5631.pool.root
PHYS_TTBAR_MC21=mc21_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.DAOD_PHYS_10evts.e8453_s3873_r13829_p5855.pool.root
PHYS_JETS_MC21=mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.DAOD_PHYS_10evts.e8453_s3873_r13829_p5855.pool.root
PHYS_JETS_MC23=mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.DAOD_PHYS_10evts.e8514_s4111_r14622_p5706.pool.root
PHYS_JETS_MC23_AF3=mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.DAOD_PHYS_10evts.e8514_s4114_r14908_p5855.pool.root

PHYS_YYBB_MC20=mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0.DAOD_PHYS_10evts.e8222_s3681_r13144_p5855.pool.root
PHYSLITE_YYBB_MC20=mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0.DAOD_PHYSLITE_10evts.e8222_s3681_r13144_p5855.pool.root

PHYS_BBTT_HH_MC21=mc21_13p6TeV.601477.PhPy8EG_HHbbttHadHad_cHHH01d0.DAOD_PHYS_10evts.e8472_s3873_r13829_p5855.pool.root
PHYSLITE_BBTT_HH_MC21=mc21_13p6TeV.601477.PhPy8EG_HHbbttHadHad_cHHH01d0.DAOD_PHYSLITE_10evts.e8472_s3873_r13829_p5855.pool.root
# bbtt: using hadhad mc21 samples for the lephad test as we don't have the lephad mc21 samples yet
PHYS_BBTT_LH_MC21=${PHYS_BBTT_HH_MC21}
PHYSLITE_BBTT_LH_MC21=${PHYSLITE_BBTT_HH_MC21}

declare -A DATAFILES=(
    [data18-fast]=${PHYS_DATA_2018}
    [lite-data18-fast]=${PHYSLITE_DATA_2018}
    [data18]=${PHYS_DATA_2018}
    [data22-fast]=${PHYS_DATA_2022}
    [lite-data22-fast]=${PHYSLITE_DATA_2022}
    [jets-mc20-fast]=${PHYS_JETS_MC20}
    [jets-mc20]=${PHYS_JETS_MC20}
    [ttbar-mc20-fast]=${PHYS_TTBAR_MC20}
    [ttbar-mc20]=${PHYS_TTBAR_MC20}
    [hh4b-mc21]=${PHYS_HH4B_MC21}
    [hh4b-resolved-mc21]=${PHYS_HH4B_MC21}
    [sh4b-boosted-mc20-fast]=${PHYS_SH4B_BOOSTED_MC20}
    [ttbar-mc21-fast]=${PHYS_TTBAR_MC21}
    [ttbar-mc21]=${PHYS_TTBAR_MC21}
    [jets-mc21]=${PHYS_JETS_MC21}
    [jets-mc23]=${PHYS_JETS_MC23}
    [jets-mc23-af3]=${PHYS_JETS_MC23_AF3}
    [lite-sh4b-boosted-mc20-fast]=${PHYSLITE_SH4B_BOOSTED_MC20}
    [lite-sh4b-boosted-mc20]=${PHYSLITE_SH4B_BOOSTED_MC20}
    [lite-hh4b-mc21-fast]=${PHYSLITE_HH4B_MC21}
    [lite-hh4b-mc21]=${PHYSLITE_HH4B_MC21}
    [sh4b-boosted-mc20]=${PHYS_SH4B_BOOSTED_MC20}
    [sh4b-boosted-mc20-p5658]=${PHYS_SH4B_BOOSTED_MC20_p5658}
    [yybb-mc20]=${PHYS_YYBB_MC20}
    [lite-yybb-mc20]=${PHYSLITE_YYBB_MC20}
    [lite-bbtt-hadhad-mc21]=${PHYSLITE_BBTT_HH_MC21}
    [bbtt-hadhad-mc21]=${PHYS_BBTT_HH_MC21}
    [lite-bbtt-lephad-mc21]=${PHYSLITE_BBTT_LH_MC21}
    [bbtt-lephad-mc21]=${PHYS_BBTT_LH_MC21}
    # systematics enabled
    [syst-ttbar-mc21]=${PHYS_TTBAR_MC21}
    [syst-hh4b-mc21]=${PHYS_HH4B_MC21}
    # non-ntupler workflows
    [aod-hh4b-mc21-fast]=${PHYS_HH4B_MC21}
    [h5-hh4b-mc21]=${PHYS_HH4B_MC21}
    [hist-syst-hh4b-mc21]=${PHYS_HH4B_MC21}
    [awkward-sh4b-boosted-mc20-fast]=${PHYS_SH4B_BOOSTED_MC20}
    # test mode
    [fail]=${PHYS_HH4B_MC21}
)

declare -A TESTS=(
    [data18-fast]="data-fast easyjet-ntupler"
    [lite-data18-fast]="data-fast easyjet-ntupler"
    [data22-fast]="data-fast easyjet-ntupler"
    [lite-data22-fast]="data-fast easyjet-ntupler"
    [jets-mc20-fast]="fast easyjet-ntupler"
    [jets-mc20]="simple easyjet-ntupler"
    [ttbar-mc20-fast]="fast easyjet-ntupler"
    [ttbar-mc20]="simple easyjet-ntupler"
    [sh4b-boosted-mc20-fast]="fast easyjet-ntupler"
    [hh4b-mc21]="simple easyjet-ntupler"
    [hh4b-resolved-mc21]="simple bbbb-ntupler"
    [ttbar-mc21-fast]="fast easyjet-ntupler"
    [ttbar-mc21]="simple easyjet-ntupler"
    [jets-mc21]="simple easyjet-ntupler"
    [jets-mc23]="simple easyjet-ntupler"
    [jets-mc23-af3]="simple easyjet-ntupler"
    [lite-sh4b-boosted-mc20-fast]="fast easyjet-ntupler"
    [lite-sh4b-boosted-mc20]="simple easyjet-ntupler"
    [lite-hh4b-mc21-fast]="fast easyjet-ntupler"
    [lite-hh4b-mc21]="simple easyjet-ntupler"
    [sh4b-boosted-mc20]="simple bbbb-ntupler"
    [sh4b-boosted-mc20-p5658]="simple bbbb-ntupler"
    [yybb-mc20]="simple bbyy-ntupler"
    [lite-yybb-mc20]="simple bbyy-ntupler"
    [lite-bbtt-hadhad-mc21]="simple bbtt-ntupler"
    [bbtt-hadhad-mc21]="simple bbtt-ntupler"
    [lite-bbtt-lephad-mc21]="simple bbtt-ntupler"
    [bbtt-lephad-mc21]="simple bbtt-ntupler"
    # systamatics
    [syst-ttbar-mc21]="systematics easyjet-ntupler"
    [syst-hh4b-mc21]="systematics easyjet-ntupler"
    # non-ntupler
    [aod-hh4b-mc21-fast]="aod-fast bbbb-ntupler"
    [h5-hh4b-mc21]="h5-fast easyjet-ntupler"
    [hist-syst-hh4b-mc21]="hist-syst bbbb-ntupler"
    [awkward-sh4b-boosted-mc20-fast]="fast easyjet-ntupler"
    # test mode
    [fail]=false
)

DEFAULT_CONFIG=EasyjetHub/RunConfig.yaml
declare -A CONFIGS=(
    [lite-data18-fast]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [lite-data22-fast]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [lite-sh4b-boosted-mc20-fast]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [lite-sh4b-boosted-mc20]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [lite-hh4b-mc21-fast]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [lite-hh4b-mc21]=EasyjetHub/RunConfig-PHYSLITE.yaml
    [sh4b-boosted-mc20]=bbbbAnalysis/RunConfig-SH4b.yaml
    [sh4b-boosted-mc20-p5658]=bbbbAnalysis/RunConfig-SH4b.yaml
    [hh4b-resolved-mc21]=bbbbAnalysis/RunConfig-Resolved.yaml
    [yybb-mc20]=bbyyAnalysis/RunConfig-PHYS-yybb.yaml
    [lite-yybb-mc20]=bbyyAnalysis/RunConfig-PHYSLITE-yybb.yaml
    [lite-bbtt-hadhad-mc21]=bbttAnalysis/RunConfig-PHYSLITE-bbtt-bypass.yaml
    [bbtt-hadhad-mc21]=bbttAnalysis/RunConfig-PHYS-bbtt-bypass.yaml
    [lite-bbtt-lephad-mc21]=bbttAnalysis/RunConfig-PHYSLITE-bbtt-bypass.yaml
    [bbtt-lephad-mc21]=bbttAnalysis/RunConfig-PHYS-bbtt-bypass.yaml
    # non-ntupler
    [aod-hh4b-mc21-fast]=bbbbAnalysis/RunConfig-Resolved.yaml
    [hist-syst-hh4b-mc21]=bbbbAnalysis/histonly-config.yaml
    [awkward-sh4b-boosted-mc20-fast]=EasyjetHub/awkward-config.yaml
)

# some common variables in the tests
COMMON="-O"

# specific tests
simple() {
    $1 $2 -c $3 $COMMON -o analysis.root
    metadata-check -v
}
systematics() {
    $1 $2 -c $3 -o $COMMON --do-CP-systematics True
    metadata-check -v
}
fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" -o analysis.root --h5-output test.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 30 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
data-fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" -o analysis.root --h5-output test.h5"
    OPTS+=" --do-trigger-filtering False"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 30 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
h5-fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --h5-output test.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 30 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
aod-fast() {
    local AOD=test.pool.root
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --output-xaod ${AOD}"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 30 "
    echo "running $CMD"
    $CMD
    metadata-check -v
    local HISTS=aod-hists.h5
    CMD="bbbb-hists ${AOD} -o $HISTS"
    echo "running $CMD"
    $CMD
    echo "made hists:"
    h5ls ${HISTS}/nominal
}
hist-syst() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --output-hists hists.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+="  --timeout 30"
    echo "running $CMD"
    $CMD
    metadata-check -v
}

################################################
# parse arguments
################################################

ALL_MODES=${!TESTS[*]}
DIRECTORY=${EASYJET_TESTDIR-""}
DATA_URL=https://gitlab.cern.ch/easyjet/hh4b-test-files/-/raw
LOG_LEVEL=${EASYJET_LOG_LEVEL-"WARNING"}
NPROCS=${EASYJET_NPROCS-$(nproc)}
# metatest means we stuff in an echo prefix to commands
if [[ ${EASYJET_METATEST+x} ]]; then
    METASTATUS=set
    PFX=echo
else
    METASTATUS=unset
    PFX=""
fi

print-usage() {
    echo "usage: ${0##*/} [-h] [-l <level>] [-L <log-file>] [-d <dir>] <mode>" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and run it.

Options:
 -d <dir>: specify directory to run in
 -l <level>: set log level
 -L <log-file>: specify log file into which to direct output
 -h: print help

Run modes:
$(dump-modes)
  all -> run all of the above (for local testing)
  all-exit-early -> all, but exit immediately if any test fails

Notes on environment variables:
 - EASYJET_LOG_LEVEL: default for -l, currently ${LOG_LEVEL}
 - EASYJET_TESTDIR: default for -d, currently ${DIRECTORY:-/tmp/<random>}
 - EASYJET_NPROCS: number of processes with "all", currently ${NPROCS}
 - EASYJET_METATEST: set to run a test test, currently ${METASTATUS}

EOF
    exit 1
}

# Terminal magic: if stdout is set to go to a terminal we'll add
# colors to the help output below. Otherwise we strip this stuff
# out. Note that you have to check the interactivity here: within the
# function the output stream is never seen as going to the terminal.
if [[ -t 1 ]]; then
    INTERACTIVE=yes
else
    INTERACTIVE=""
fi
dump-modes() {
    if [[ ${INTERACTIVE} ]]; then
        local RB=$(tput setaf 1)$(tput bold)
        local NO=$(tput sgr0)
        local Y=$(tput setaf 3)
        local G=$(tput setaf 2)
        local FMT="  %s ${Y}-> ${RB}%s${NO} ${G}%s${NO} %s\n"
    else
        local FMT="  %s -> %s %s %s\n"
    fi
    local mode
    for mode in ${ALL_MODES[*]/fail}
    do
        local func=${TESTS[$mode]}
        local config=${CONFIGS[$mode]-$DEFAULT_CONFIG}
        local data=${DATAFILES[$mode]}
        printf "${FMT}" $mode "$func" $config $data
    done
}

OPT_KEYS=":d:hl:L:"
dump-complete() {
    printf "%s " ${ALL_MODES[*]} all all-exit-early
    local char
    echo -n ${OPT_KEYS//:} | while read -n 1 char
    do
        printf -- "-%s " $char
    done
}

# the c option is "hidden", we just use it to pass options to tab
# complete
while getopts ${OPT_KEYS}c o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        h) help ;;
        l) LOG_LEVEL=${OPTARG} ;;
        L) LOG_FILE=${OPTARG} ;;
        # this is just here for tab complete
        c) dump-complete; exit 0 ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1
if [[ $MODE == "all-exit-early" ]]; then
    MODE="all"
    EXIT_EARLY=1
fi

# add common options
COMMON+=" -l ${LOG_LEVEL}"

############################################
# Check that all the modes / paths exist
############################################
#

verify-test() {
    if [[ ! ${DATAFILES[$1]+x} ]]; then usage; fi
    if [[ ! ${TESTS[$1]+x} ]]; then usage; fi
}

if [[ ${MODE} == "all" ]]; then
    for mode in $ALL_MODES
    do
        verify-test $mode
    done
else
    verify-test $MODE
fi

#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

error-handler() {
    echo "$1 failed with exit code $?" >> test_results.log
}

run-test() {
    local mode=$1
    local download_path=${DATAFILES[$mode]}
    local file=${download_path##*/}
    local config=${CONFIGS[$mode]-$DEFAULT_CONFIG}

    # get files
    if [[ ! -f ${file} ]] ; then
        echo "getting file ${file} from ${DATA_URL}"
        ${PFX} curl -s ${DATA_URL}/${TAG}/${download_path} > ${file}
    fi

    local testcmd="${PFX} ${TESTS[$mode]} $file $config"
    trap "error-handler $mode" ERR
    if [[ ! -z ${LOG_FILE+x} ]]; then
        $testcmd > $LOG_FILE 2>&1
    else
        $testcmd
    fi
    echo "$mode succeeded" >> test_results.log
}

test-summary() {
    if [[ ! -z ${EXIT_EARLY+x} ]] ; then
        echo "Waiting 10s to collect test results from other incomplete jobs"
        sleep 10
    fi
    printf "\nAvailable test results:\n\n"
    for result in */test_results.log; do
        cat $result >> test_results.log
    done
    cat test_results.log
}

# now run the test
if [[ ${MODE} == "all" ]] ; then
    # Run in parallel -- each job is in a separate shell
    testcmd="${0} -d {} -L test.log {}"
    if [[ ! -z ${EXIT_EARLY+x} ]] ; then
        testcmd+=" || exit 255"
    fi
    PMODES=${ALL_MODES[*]/fail}
    trap "echo failed tests detected; test-summary" ERR
    printf "%s\n" $PMODES | xargs -P $NPROCS -I {} bash -c "${testcmd}" &> /dev/null

    printf "\nAll tests completed successfully\n"
else
    echo "running ${MODE}"
    trap "echo test failed with exit code \$?" ERR
    run-test $MODE
    echo "${MODE} completed successfully"
fi

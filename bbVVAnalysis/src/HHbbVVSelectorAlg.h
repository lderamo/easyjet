/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

// Always protect against multiple includes!
#ifndef BBVVANALYSIS_HHBBVVSELECTORALG
#define BBVVANALYSIS_HHBBVVSELECTORALG

#include <memory>

#include "AnaAlgorithm/AnaAlgorithm.h"
#include <FourMomUtils/xAODP4Helpers.h>
#include <AsgDataHandles/ReadDecorHandleKey.h>

#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/ISysHandleBase.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>
#include <SystematicsHandles/SysReadDecorHandle.h>
#include <AsgDataHandles/WriteDecorHandleKey.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMissingET/MissingETContainer.h>

#include <SystematicsHandles/SysFilterReporterParams.h>


namespace HHBBVV
{

  enum Channel
  {
    splitboosted1lep = 0,
    boosted1lep = 1,
    splitboosted0lep = 2,
    boosted0lep = 3,
  };

  /// \brief An algorithm for counting containers
  class HHbbVVSelectorAlg final : public EL::AnaAlgorithm
  {
    /// \brief The standard constructor
public:
    HHbbVVSelectorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// \brief Finalisation method, for cleanup, final print out etc
    StatusCode finalize() override;

private:

    /// \brief Steerable properties
    Gaudi::Property<std::vector<std::string>> m_channel_names
      { this, "channel", {}, "Which channel to run" };

    std::vector<HHBBVV::Channel> m_channels;

    Gaudi::Property<bool> m_bypass
      { this, "bypass", false, "Run selector algorithm in pass-through mode" };

    /// \brief Setup syst-aware input container handles
    CP::SysListHandle m_systematicsList {this};

    CP::SysReadHandle<xAOD::JetContainer>
      m_jetHandle{ this, "jets", "",   "Jet container to read" };

    CP::SysReadHandle<xAOD::JetContainer>
      m_lrjetHandle{ this, "lrjets", "",   "Lerge-R jet container to read" };
    
    CP::SysReadHandle<xAOD::ElectronContainer>
    m_electronHandle{ this, "electrons", "",   "Electron container to read" };

    CP::SysReadHandle<xAOD::MuonContainer>
    m_muonHandle{ this, "muons", "",   "Muon container to read" };

    CP::SysReadHandle<xAOD::MissingETContainer>
    m_metHandle{ this, "met", "AnalysisMET",   "MET container to read" };

    CP::SysReadHandle<xAOD::EventInfo>
    m_eventHandle{ this, "event", "EventInfo",   "EventInfo container to read" };

    CP::SysReadDecorHandle<char> 
    m_isBtag {this, "bTagWPDecorName", "", "Name of input dectorator for b-tagging"};

    Gaudi::Property<std::string> m_eleIdDecorName
      { this, "eleIdDecorKey", "DFCommonElectronsLHTight","Decoration for electron ID working point" };
    SG::ReadDecorHandleKey<xAOD::ElectronContainer> m_eleIdDecorKey;
		      
    Gaudi::Property<std::string> m_muonIdDecorName
      { this, "muonIdDecorKey", "DFCommonMuonPassIDCuts","Decoration for muon ID cuts" };
    SG::ReadDecorHandleKey<xAOD::MuonContainer> m_muonIdDecorKey;

    Gaudi::Property<std::string> m_muonPreselDecorName
      { this, "muonPreselDecorKey", "DFCommonMuonPassPreselection","Decoration for muon preselection" };
    SG::ReadDecorHandleKey<xAOD::MuonContainer> m_muonPreselDecorKey;

    /// \brief Setup sys-aware output decorations
    CP::SysWriteDecorHandle<bool> m_pass_sr {"pass_bbVV_sr_%SYS%", this};

    CP::SysFilterReporterParams m_filterParams {this, "HHbbVV selection"};

    CP::SysWriteDecorHandle<bool> m_selected_el {"selected_el_%SYS%", this};
    CP::SysWriteDecorHandle<bool> m_selected_mu {"selected_mu_%SYS%", this};
    
    /// \brief Internal variables
    // TODO: implement internal and relevant bbVV variables here
    bool TWO_JETS;


  };
}

#endif

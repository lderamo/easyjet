4b Analysis Code
================

This package contains code specific to the HH/SH -> 4b analyses. It should not depend directly on any other `*Analysis` packages.
